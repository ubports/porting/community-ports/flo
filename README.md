# Device image for flo

This repository contains the *prebuilt*, *binary* objects which are packed into the device tree for the Nexus 7 2013 Wi-Fi, codenamed `flo`.


## Sources

### [system/var/lib/lxc/android/system.img](system/var/lib/lxc/android/system.img)

The Android source tree originally used to build the system image was previously hosted on the Canonical Phablet Gerrit instance, which is no longer available. It can now be found at https://github.com/ubports-android, though a full manifest is not available.

The kernel source used to build the kernel objects contained in this image can be found at https://github.com/ubports/ubuntu_kernel_xenial/tree/flo. Short build instructions can be found at the top of its README.

### [partitions/](partitions/)

The kernel source used to build the kernel images contained in [partitions/boot.img](partitions/boot.img) and [partitions/recovery.img](partitions/recovery.img) can be found at https://github.com/ubports/ubuntu_kernel_xenial/tree/flo. Short build instructions can be found at the top of its README.